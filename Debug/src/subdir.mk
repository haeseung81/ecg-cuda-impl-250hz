################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/IIR_bandpassfilter.cpp \
../src/IIR_bandpassfilter_emxAPI.cpp \
../src/IIR_bandpassfilter_emxutil.cpp \
../src/IIR_bandpassfilter_initialize.cpp \
../src/IIR_bandpassfilter_terminate.cpp \
../src/caps.cpp \
../src/common.cpp \
../src/file_io.cpp \
../src/filter.cpp \
../src/main.cpp \
../src/peak_detector.cpp \
../src/qrsdet.cpp \
../src/qrsfilt.cpp \
../src/rtGetInf.cpp \
../src/rtGetNaN.cpp \
../src/rt_nonfinite.cpp 

CU_SRCS += \
../src/cucaps.cu 

CU_DEPS += \
./src/cucaps.d 

OBJS += \
./src/IIR_bandpassfilter.o \
./src/IIR_bandpassfilter_emxAPI.o \
./src/IIR_bandpassfilter_emxutil.o \
./src/IIR_bandpassfilter_initialize.o \
./src/IIR_bandpassfilter_terminate.o \
./src/caps.o \
./src/common.o \
./src/cucaps.o \
./src/file_io.o \
./src/filter.o \
./src/main.o \
./src/peak_detector.o \
./src/qrsdet.o \
./src/qrsfilt.o \
./src/rtGetInf.o \
./src/rtGetNaN.o \
./src/rt_nonfinite.o 

CPP_DEPS += \
./src/IIR_bandpassfilter.d \
./src/IIR_bandpassfilter_emxAPI.d \
./src/IIR_bandpassfilter_emxutil.d \
./src/IIR_bandpassfilter_initialize.d \
./src/IIR_bandpassfilter_terminate.d \
./src/caps.d \
./src/common.d \
./src/file_io.d \
./src/filter.d \
./src/main.d \
./src/peak_detector.d \
./src/qrsdet.d \
./src/qrsfilt.d \
./src/rtGetInf.d \
./src/rtGetNaN.d \
./src/rt_nonfinite.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: NVCC Compiler'
	/usr/local/cuda-6.0/bin/nvcc -G -g -O0 -gencode arch=compute_30,code=sm_30 -gencode arch=compute_32,code=sm_32  -odir "src" -M -o "$(@:%.o=%.d)" "$<"
	/usr/local/cuda-6.0/bin/nvcc -G -g -O0 --compile  -x c++ -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/%.o: ../src/%.cu
	@echo 'Building file: $<'
	@echo 'Invoking: NVCC Compiler'
	/usr/local/cuda-6.0/bin/nvcc -G -g -O0 -gencode arch=compute_30,code=sm_30 -gencode arch=compute_32,code=sm_32  -odir "src" -M -o "$(@:%.o=%.d)" "$<"
	/usr/local/cuda-6.0/bin/nvcc --compile -G -O0 -g -gencode arch=compute_30,code=compute_30 -gencode arch=compute_30,code=sm_30 -gencode arch=compute_32,code=compute_32  -x cu -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


