/*
 * filter.h
 *
 * Code generation for function 'filter'
 *
 * C source code generated on: Mon Jan 12 12:33:15 2015
 *
 */

#ifndef __FILTER_H__
#define __FILTER_H__
/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "rtwtypes.h"
#include "IIR_bandpassfilter_types.h"

/* Function Declarations */
extern void filter(emxArray_real_T *x, real_T zi[10], emxArray_real_T *y, real_T* coeff_a, real_T* coeff_b);
#endif
/* End of code generation (filter.h) */
