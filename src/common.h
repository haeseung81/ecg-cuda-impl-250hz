/*
 * common.h
 *
 *  Created on: Jan 6, 2015
 *      Author: haeseunl
 */

#ifndef COMMON_H_
#define COMMON_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <sys/time.h>
#include <unistd.h>
#include <vector>

typedef unsigned long CLK;

typedef struct _app_setting_t
{
	int    template_size;
	float  threshold;
	int    sample_rate;
	int    input_interval_ms;
	int    num_channel;
	int    sensitivity_resolution;
	char*  input_file;
	char*  output_file;
} app_setting_t;


/**
 * This macro checks return value of the CUDA runtime call and exits
 * the application if the call failed.
 */
#define CUDA_CHECK_RETURN(value) {											\
	cudaError_t _m_cudaStat = value;										\
	if (_m_cudaStat != cudaSuccess) {										\
		fprintf(stderr, "Error %s at line %d in file %s\n",					\
				cudaGetErrorString(_m_cudaStat), __LINE__, __FILE__);		\
		exit(1);															\
	}}

CLK GetBaseTimeUs(void);
unsigned int GetTimeUs(CLK base);


void FindMaxFromRange(float* input, int start, int end, float* max_value, int* index);
float GetAvg(float* input, int num);
float* CropMemAlloc(int size, int zero_pad_num);
void CropData(float* output, int start, int end, float* input, int input_num, short indicator, int zero_pad_num);

extern app_setting_t prog_setting;


#endif /* COMMON_H_ */
