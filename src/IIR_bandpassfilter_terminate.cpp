/*
 * IIR_bandpassfilter_terminate.c
 *
 * Code generation for function 'IIR_bandpassfilter_terminate'
 *
 * C source code generated on: Mon Jan 12 12:33:15 2015
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "IIR_bandpassfilter.h"
#include "IIR_bandpassfilter_terminate.h"

/* Function Definitions */
void IIR_bandpassfilter_terminate(void)
{
  /* (no terminate code required) */
}

/* End of code generation (IIR_bandpassfilter_terminate.c) */
