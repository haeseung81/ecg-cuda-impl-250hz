/*
 * IIR_bandpassfilter_emxutil.h
 *
 * Code generation for function 'IIR_bandpassfilter_emxutil'
 *
 * C source code generated on: Mon Jan 12 12:33:15 2015
 *
 */

#ifndef __IIR_BANDPASSFILTER_EMXUTIL_H__
#define __IIR_BANDPASSFILTER_EMXUTIL_H__
/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "rtwtypes.h"
#include "IIR_bandpassfilter_types.h"

/* Function Declarations */
extern void b_emxInit_real_T(emxArray_real_T **pEmxArray, int32_T numDimensions);
extern void emxEnsureCapacity(emxArray__common *emxArray, int32_T oldNumel, int32_T elementSize);
extern void emxFree_int32_T(emxArray_int32_T **pEmxArray);
extern void emxFree_real_T(emxArray_real_T **pEmxArray);
extern void emxInit_int32_T(emxArray_int32_T **pEmxArray, int32_T numDimensions);
extern void emxInit_real_T(emxArray_real_T **pEmxArray, int32_T numDimensions);
#endif
/* End of code generation (IIR_bandpassfilter_emxutil.h) */
