#include "common.h"




CLK GetBaseTimeUs(void)
{
	struct timeval tv;
	gettimeofday(&tv,NULL);
	return tv.tv_sec*(CLK)1000000+tv.tv_usec;
}

unsigned int GetTimeUs(CLK base)
{
	struct timeval tv;
	CLK current;

	gettimeofday(&tv,NULL);
	current = tv.tv_sec*(CLK)1000000+tv.tv_usec-base;

	return (unsigned int)current;
}


#define DBG_FIND_MAX_RGE_
#ifdef DBG_FIND_MAX_RGE
#define PNT_FIND_MAX_RGE(...) printf(__VA_ARGS__)
#else
#define PNT_FIND_MAX_RGE(...)
#endif

void FindMaxFromRange(float* input, int start, int end, float* max_value, int* index)
{
	int i;
	int idx = 0;
	float max = 0;

	// find max value and its index
	for(i=start; i<end; i++) {
		if (max<input[i]) {
			max = input[i];
			idx = i;
		}
	}

	PNT_FIND_MAX_RGE("initial max idx: %d (%f)\n", idx, input[i]);
	// return max value
	*max_value = max;
	if (index!=NULL) { *index = idx; }
}

float GetAvg(float* input, int num)
{
	float avg = 0;
	int i=0;

	for (i=0; i<num; i++) {
		avg += input[i];
	}

	return (avg/num);
}


float* CropMemAlloc(int size, int zero_pad_num)
{
	int size_per_ch  = size + (zero_pad_num*2);
	float* data = NULL;

	data = (float*)malloc(sizeof(float)*size_per_ch*12);
	memset(data, (float)0, sizeof(float)*size_per_ch*12);

	assert(data!=NULL);
	return data;
}


void CropData(float* output, int start, int end, float* input, int input_num, short indicator, int zero_pad_num)
{
	int crop_size = end-start+1;
	int out_offset = 0;

	int ch_cnt=0;
	short ch_flag = 0x0800;
	int i = 0;
	int j = 0;
	int out_cnt=0;

	//printf("[@CropData]\n");


	assert(start>=0);
	assert(end>=0);

	for (i=0; i<12; i++) {
		if ((indicator&ch_flag)!=0x0000) {

		}

		//printf("Use channel (%d)\n", i);
		ch_cnt++;

		out_offset = zero_pad_num + i*(crop_size+(2*zero_pad_num));
		out_cnt=0;
		for (j=start; j<=end; j++) {
			//printf("input[%d] -> data[%d]\n", i*input_num+j, out_offset+j-start);
			assert((input_num*12)>i*input_num+j);
			output[out_offset+out_cnt] = input[i*input_num+j];
			//printf("output[%d]: %f <- input[%d]: %f (input_num: %d)\n", out_offset+out_cnt, output[out_offset+out_cnt], i*input_num+j, input[i*input_num+j], input_num);
			out_cnt++;
		}

		ch_flag = ch_flag>>1;
	}
}
