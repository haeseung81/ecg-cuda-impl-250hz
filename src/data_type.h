/*
 * cummulative_data.h
 *
 *  Created on: Jan 14, 2015
 *      Author: haeseunl
 */

#ifndef DATA_TYPE_H_
#define DATA_TYPE_H_

#define TH_BUF_SIZE 8

typedef struct _central_data_t {
	int buf_cnt;
	int sig_next_idx;
	int noise_next_idx;
	int rr_next_idx;
	float sig_peak_buf[TH_BUF_SIZE];
	float noise_peak_buf[TH_BUF_SIZE];
	float rr_buf[TH_BUF_SIZE];

} central_data_t;


#endif /* DATA_TYPE_H_ */
