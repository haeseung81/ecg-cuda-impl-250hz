#include "common.h"
#include "peak_detector.h"

#include "IIR_bandpassfilter.h"
#include "IIR_bandpassfilter_types.h"
#include "IIR_bandpassfilter_emxAPI.h"
#include "IIR_bandpassfilter_coeff.h"
#include "file_io.h"

#include "qrs_c_header.h"

extern central_data_t peak_buf;

int* QRSDetect(int* qrs_data_num, int sampling_rate, float* input, int input_num, int init_sec)
{
	int*   qrs_output=NULL;
	int*   peak_idx;
	float* peak_mag;
	int* tmp_output;
	int output_num;
	int i;
	int n;
	int peak_idx_num;
	float threshold;



	int window_size = 0;
	float* tmp_filter_output = (float*)malloc(sizeof(float)*input_num);
	float* diff_filter_output = (float*)malloc(sizeof(float)*input_num-1);
	float* integral_data = (float*)malloc(sizeof(float)*input_num-1);
	float* integral_ecg;

	// number of output estimation
	output_num = ceil((float)input_num/sampling_rate * 150);
	tmp_output = (int*)malloc(sizeof(int)*output_num);

	// change to memset
	for(i=0; i<output_num;i++) {
		tmp_output[i] = 0;
	}

	// filtering
	emxArray_real_T *filter_input;
	emxArray_real_T *filter_output;

	filter_input = emxCreate_real_T(1, input_num);
	filter_output = emxCreate_real_T(1, input_num);

	for(i=0; i<input_num; i++) {
		filter_input->data[i] = (double)input[i];
	}

	IIR_bandpassfilter(filter_input, filter_output, second_coeff_a, second_coeff_b, second_coeff_c);

	for(i=0; i<input_num; i++) {
		tmp_filter_output[i] = (float)filter_output->data[i];
		//printf("output_f[%d]: %f\n", i, output_f[i]);
	}

	//TempDump("f_out.bin", tmp_filter_output, input_num);


	// differential filter
	for(n=0; n<(input_num-1); n++) {
		diff_filter_output[n] = tmp_filter_output[n+1] - tmp_filter_output[n];
		//printf("output_f[%d]: %f\n", i, output_f[i]);
	}

	//TempDump("diff_out.bin", diff_filter_output, (input_num-1));

	// square calculation
	for(n=0; n<(input_num-1); n++) {
		diff_filter_output[n] = diff_filter_output[n]*diff_filter_output[n];
		//printf("output_f[%d]: %f\n", i, output_f[i]);
	}

	//TempDump("sq_out.bin", diff_filter_output, (input_num-1));

	// integral image
	integral_data[0] = diff_filter_output[0];
	for(n=1; n<(input_num-1); n++) {
		integral_data[n] = integral_data[n-1] + diff_filter_output[n];
		//printf("output_f[%d]: %f\n", i, output_f[i]);
	}

	//TempDump("int_out.bin", integral_data, (input_num-1));

	// clear
//	for (n=0; n<(input_num-1); n++) {
//		if (n>0) {
//			assert(integral_data[n]>=integral_data[n-1]);
//		}
//		printf("integral_data[%d]: %f\n", n, integral_data[n]);
//	}

	//////////////////////////////////////////////////////////////////////////////////
	window_size = (int)(sampling_rate*0.08);
	integral_ecg = (float*)malloc(sizeof(float)*input_num-1);

	//printf("window_size: %d | input_num-1: %d\n", window_size, input_num-1);
	//integral_ecg[0] = integral_data[0];
	for(n=0; n<window_size; n++) {
		integral_ecg[n] = (integral_data[n])/(n+1);
		//printf("integral_ecg[%d]: %f\n", n, integral_ecg[n]);
	}

	for(n=window_size; n<(input_num-1); n++) {
		integral_ecg[n] = (integral_data[n] -integral_data[n-window_size])/window_size;
	}

	//TempDump("int_ecg_out.bin", integral_data, (input_num-1));

//	for (n=0; n<(input_num-1); n++) {
//		printf("integral_ecg[%d]: %f\n", n, integral_ecg[n]);
//	}


	if (peak_buf.buf_cnt<=(TH_BUF_SIZE-1)) {
		printf("[QRSDetect] Fill buffer up to (%d) sec: peak_buf.buf_cnt: %d\n", (TH_BUF_SIZE-1), peak_buf.buf_cnt);
		InitBuf(&peak_buf, integral_ecg, sampling_rate, init_sec);
	}





	threshold = UpdateThreshold(&peak_buf);

	PeakDetect(&peak_buf, integral_ecg, sampling_rate, input_num, &peak_idx_num, &peak_idx, &peak_mag);


	qrs_output = CheckPeak(qrs_data_num, &peak_buf, sampling_rate, peak_idx, peak_mag, peak_idx_num, integral_ecg, threshold);
	assert(qrs_output!=NULL);


	return qrs_output;
}


#define DBG_INIT_BUF
#ifdef DBG_INIT_BUF
#define PNT_DBG_INIT_BUF(...) printf(__VA_ARGS__)
#else
#define PNT_DBG_INIT_BUF(...)
#endif


void InitBuf(central_data_t* peak_buf, float* input, int sampling_rate, int init_sec)
{
	int i;
	float peak;
	for (i=0; i<init_sec; i++) {

		if (peak_buf->buf_cnt<=(TH_BUF_SIZE-1)) {
			assert(peak_buf->buf_cnt<TH_BUF_SIZE);
			//FindMaxFromRange(input, i*sampling_rate, (i*sampling_rate)+sampling_rate, &peak, NULL);

			PNT_DBG_INIT_BUF("[InitBuf] (%d)th peak data: %f\n", peak_buf->buf_cnt, peak);
			peak_buf->sig_peak_buf[peak_buf->buf_cnt] = peak;
			peak_buf->noise_peak_buf[peak_buf->buf_cnt] = 0;
			peak_buf->rr_buf[peak_buf->buf_cnt] = 1;
			peak_buf->buf_cnt++;
		}

	}
}


#define UPDATE_TH_DEBUG_
#ifdef UPDATE_TH_DEBUG
#define UPDATE_TH_P(...) printf(__VA_ARGS__)
#else
#define UPDATE_TH_P(...)
#endif

float UpdateThreshold(central_data_t* peak_buf)
{
	int i;
	float noise = 0;
	float signal = 0;
	float threshold = 0;

	UPDATE_TH_P("\n[UpdateThreshold] start... (peak_buf->buf_cnt: %d)\n", peak_buf->buf_cnt);

	for (i=0; i<peak_buf->buf_cnt; i++) {
		UPDATE_TH_P("peak_buf->sig_peak_buf[%d]  : %f\n", i, peak_buf->sig_peak_buf[i]);
		UPDATE_TH_P("peak_buf->noise_peak_buf[%d]: %f\n", i, peak_buf->noise_peak_buf[i]);

		signal += peak_buf->sig_peak_buf[i];
		noise += peak_buf->noise_peak_buf[i];
	}

	UPDATE_TH_P("peak_buf->buf_cnt: %d\n", peak_buf->buf_cnt);

	signal = signal / peak_buf->buf_cnt;
	noise  = noise / peak_buf->buf_cnt;

	threshold = noise + 0.3125*(signal-noise);

	return threshold;
}

#define USE_STL


int FindAllPeak(float* integral_ecg, int input_num, int** all_peak_idx, float** all_peak_mag)
{
	int all_peak_cnt = 0;
	int i;

#ifdef USE_STL
	std::vector<int> all_peak_idx_buf;
	std::vector<float> all_peak_mag_buf;
#else
	int all_peak_idx_buf[1000];
	int all_peak_mag_buf[1000];
#endif

	//
	for (i=1; i<input_num-1; i++) {
		if (integral_ecg[i-1] < integral_ecg[i] && integral_ecg[i] > integral_ecg[i+1]) {
			all_peak_cnt++;
#ifdef USE_STL
			all_peak_idx_buf.push_back(i);
			all_peak_mag_buf.push_back(integral_ecg[i]);
#else
			all_peak_idx_buf[all_peak_cnt] = i;
			all_peak_mag_buf[all_peak_cnt] = input[i];
#endif

		}
	}

	assert(all_peak_idx_buf.size()==(unsigned int)all_peak_cnt);
	assert(all_peak_idx_buf.size()==all_peak_mag_buf.size());

	*all_peak_idx = (int*)malloc(sizeof(int)*all_peak_cnt);
	*all_peak_mag = (float*)malloc(sizeof(float)*all_peak_cnt);


	for (i=0; i<all_peak_cnt; i++) {
		(*all_peak_idx)[i] = all_peak_idx_buf[i];
		(*all_peak_mag)[i] = all_peak_mag_buf[i];
	}


	return all_peak_cnt;

}


void PeakDetect(central_data_t* peak_buf, float* integral_ecg, int sampling_rate, int input_num
		, int* peak_num, int** peak_idx_output, float** peak_mag_output)
{
	int i=0;
	int all_peak_cnt;
	int* all_peak_idx = NULL;
	float* all_peak_mag = NULL;


	int peak_output_cnt=0;
	int* peak_idx_output_buf=NULL;
	int* peak_mag_output_buf=NULL;

	int min_rr = 0;
	int curr_idx = 0;
	int peak_candidate_idx = 0;
	float peak_candidate_mag = 0;



	all_peak_cnt = FindAllPeak(integral_ecg, input_num, &all_peak_idx, &all_peak_mag);
	assert(all_peak_idx != NULL);
	assert(all_peak_mag != NULL);

	min_rr = (int)((float)sampling_rate*0.2);

	peak_candidate_idx = all_peak_idx[0];
	peak_candidate_mag = all_peak_mag[0];

	peak_idx_output_buf = (int*)malloc(sizeof(int)*all_peak_cnt);
	peak_mag_output_buf = (int*)malloc(sizeof(int)*all_peak_cnt);

	for (curr_idx=1; curr_idx<all_peak_cnt; curr_idx++) {
		if (all_peak_idx[curr_idx]-peak_candidate_idx <= min_rr && all_peak_mag[curr_idx] > peak_candidate_mag) {
			peak_candidate_idx = all_peak_idx[curr_idx];
			peak_candidate_mag = all_peak_mag[curr_idx];
		}
		else if (all_peak_idx[curr_idx]-peak_candidate_idx > min_rr) {
			peak_idx_output_buf[peak_output_cnt] = peak_candidate_idx;
			peak_mag_output_buf[peak_output_cnt] = peak_candidate_mag;
			peak_output_cnt++;
			peak_candidate_idx = all_peak_idx[curr_idx];
			peak_candidate_mag = all_peak_mag[curr_idx];
		}
	}

	*peak_idx_output = (int*)malloc(sizeof(int)*peak_output_cnt);
	*peak_mag_output = (float*)malloc(sizeof(float)*peak_output_cnt);




	*peak_num = peak_output_cnt;
	for(i=0; i<peak_output_cnt; i++) {
		(*peak_idx_output)[i] = peak_idx_output_buf[i];
		(*peak_mag_output)[i] = peak_mag_output_buf[i];
	}
}

extern int time_cnt;


int* CheckPeak(int* qrs_data_num, central_data_t* peak_buf, int sampling_rate
		, int* peak_idx, float* peak_mag, int peak_idx_num
		, float* integral_ecg, float threshold_intput)
{
	int i=0;
	int* final_peak=NULL;
	int final_peak_cnt = 0;
	int* final_peak_buf = (int*)malloc(sizeof(int)*peak_idx_num);
	float rr_avg = 0;
	float threshold = threshold_intput;

	float last_qrs_ms = 0;
	int   last_qrs_next_peak = 0;

	for(i=0; i<peak_idx_num; i++) {
		assert(final_peak_cnt<peak_idx_num);
		if(peak_mag[i] > threshold) {
			final_peak_buf[final_peak_cnt] = i;
			//final_peak_buf[final_peak_cnt] = peak_mag[i];
			// peak_buf update (acceptasQRS) ---------------------------------
			AcceptAsQRS(peak_buf, peak_idx[i], final_peak_buf, final_peak_cnt, peak_mag[i]);
			final_peak_cnt++;
			// --------------------------------------------------------------
		}
		else if (peak_mag[i]>(threshold/2) && final_peak_cnt>1 && peak_idx_num>(i+1)) {
			rr_avg = GetAvg(peak_buf->rr_buf, TH_BUF_SIZE);
			last_qrs_ms = (peak_idx[i] - final_peak_buf[final_peak_cnt-1]) * 1000 / sampling_rate;
			last_qrs_next_peak = peak_idx[i+1] - final_peak_buf[final_peak_cnt-1];

			if (last_qrs_ms>360 && last_qrs_next_peak>(1.5*rr_avg)) {
				// peak_buf update (acceptasQRS) ---------------------------------
				//AcceptAsQRS(peak_buf, final_peak_cnt, peak_mag[i]);
				AcceptAsQRS(peak_buf, peak_idx[i], final_peak_buf, final_peak_cnt, peak_mag[i]);
				final_peak_cnt++;
				// --------------------------------------------------------------
			}
			else {
				AcceptAsNoise(peak_buf, peak_mag[i]);
			}
		}

		threshold = UpdateThreshold(peak_buf);


	}


	final_peak = (int*)malloc(sizeof(int)*final_peak_cnt);
	for (i=0; i<final_peak_cnt; i++) {
		final_peak[i] = final_peak_buf[i] - (40*sampling_rate/1000) + 5;
		//final_peak[i] = final_peak_buf[i];
	}

	for(i=0; i<final_peak_cnt; i++) {
		printf("(%d)th peak idx: %5d (%5d)\n", i, final_peak[i], time_cnt*sampling_rate*3 + final_peak[i] );
	}

	*qrs_data_num = final_peak_cnt;
	assert(final_peak!=NULL);
	return final_peak;
}


void AcceptAsQRS(central_data_t* peak_buf, int peak_idx, int* peak_idx_buf, int final_peak_cnt, float peak_mag_input)
{
	int idx = 0;

	peak_idx_buf[final_peak_cnt] = peak_idx;

	peak_buf->sig_peak_buf[peak_buf->sig_next_idx] = peak_mag_input;

	if (final_peak_cnt>1) {
		idx = (peak_buf->sig_next_idx + (TH_BUF_SIZE-1)) % TH_BUF_SIZE;
		assert(idx>=0);
		peak_buf->rr_buf[peak_buf->rr_next_idx] = peak_buf->sig_peak_buf[peak_buf->sig_next_idx] - peak_buf->sig_peak_buf[idx];
		peak_buf->rr_next_idx = (peak_buf->rr_next_idx+1) % TH_BUF_SIZE;
	}

	peak_buf->sig_next_idx = (peak_buf->sig_next_idx+1) % TH_BUF_SIZE;
}

void AcceptAsNoise(central_data_t* peak_buf, float noise_input)
{
	//printf("[AcceptAsNoise]\n");
	//assert(0);
	peak_buf->noise_peak_buf[peak_buf->noise_next_idx] = noise_input;
	peak_buf->noise_next_idx = (peak_buf->noise_next_idx+1) % TH_BUF_SIZE;
}


int* CheckRange(int* qrs_peak_output, int* qrs_data_num, int template_size)
{
	int* peak_final = NULL;
	int start = 0;
	int size = *qrs_data_num;
	int i=0;
	int cnt=0;

//	if (qrs_peak_output[0] <= template_size) {
//		start = 1;
//		*qrs_data_num = size - 1;
//	}
//	else {
//
//	}


	peak_final = (int*)malloc(sizeof(int)*size);

	for (i=start; i<size; i++) {
		peak_final[cnt] = qrs_peak_output[i];
		cnt++;
	}



	free(qrs_peak_output);
	assert(peak_final!=NULL);
	return peak_final;
}


int* QRSDetectVer2(int* qrs_data_num, int sampling_rate, float* input, int input_num, int curr_iter)
{
	// initialize the QRS detector
	int ret = 0;
	int data_cnt = 0;
	int cnt = 0;
	int data = 0;

	int* qrs_output_buf = (int*)malloc(sizeof(int)*(input_num/sampling_rate)*4);
	int* qrs_output = NULL;

	for (cnt=0; cnt<input_num; cnt++) {
		//data = input[cnt];
		ret = QRSDet( input[cnt], 0);
		if (ret !=0) {


			if ((cnt-ret)>0) {
				assert((cnt-ret)>0);
				qrs_output_buf[data_cnt] = cnt-ret;
				data_cnt++;
			}
			//printf("[QRSDetectVer2] (%4d)th peak: %d (ret: %d | input: %d | original: %f)\n", cnt, (cnt-ret)+(input_num*(curr_iter)), ret, data, input[cnt]);
			//printf("%d, ", (cnt-ret)+(input_num*(curr_iter)));
		}
	}

	if (data_cnt>0) {
		qrs_output = (int*)malloc(sizeof(int)*data_cnt);

		for (cnt=0; cnt<data_cnt; cnt++) {
			qrs_output[cnt] = (input_num*(curr_iter)) + qrs_output_buf[cnt];
		}
	}


	free(qrs_output_buf);
	*qrs_data_num = data_cnt;
	return qrs_output;


}

