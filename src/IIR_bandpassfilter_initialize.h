/*
 * IIR_bandpassfilter_initialize.h
 *
 * Code generation for function 'IIR_bandpassfilter_initialize'
 *
 * C source code generated on: Mon Jan 12 12:33:15 2015
 *
 */

#ifndef __IIR_BANDPASSFILTER_INITIALIZE_H__
#define __IIR_BANDPASSFILTER_INITIALIZE_H__
/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "rtwtypes.h"
#include "IIR_bandpassfilter_types.h"

/* Function Declarations */
extern void IIR_bandpassfilter_initialize(void);
#endif
/* End of code generation (IIR_bandpassfilter_initialize.h) */
