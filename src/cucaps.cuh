/*
 * caps.cuh
 *
 *  Created on: Aug 24, 2015
 *      Author: haeseunl
 */

#ifndef CAPS_CUH_
#define CAPS_CUH_

#include "constants.h"
#include "cuPrintf.cuh"

#define CU_GAU_WEIGHT_SIZE		TEMPLATE_SIZE
const __constant__ float cugaussian_weight[] = { 0.0439, 0.0564, 0.0717, 0.0901, 0.1122, 0.1381, 0.1683, 0.2030, 0.2423, 0.2863,
		                    0.3347, 0.3872, 0.4433, 0.5023, 0.5633, 0.6251, 0.6865, 0.7461, 0.8026, 0.8543,
		                    0.8999, 0.9382, 0.9680, 0.9884, 0.9987, 0.9987, 0.9884, 0.9680, 0.9382, 0.8999,
		                    0.8543, 0.8026, 0.7461, 0.6865, 0.6251, 0.5633, 0.5023, 0.4433, 0.3872, 0.3347,
		                    0.2863, 0.2423, 0.2030, 0.1683, 0.1381, 0.1122, 0.0901, 0.0717, 0.0564, 0.0439};

__device__ void cuCropData(float* output, int start, int end, float* input, int input_num, short indicator, int zero_pad_num)
{
	int crop_size = end-start+1;
	int out_offset = 0;

	int ch_cnt=0;
	short ch_flag = 0x0800;
	int i = 0;
	int j = 0;
	int out_cnt=0;

	//printf("[@CropData]\n");


	assert(start>=0);
	assert(end>=0);

	for (i=0; i<12; i++) {
//		if ((indicator&ch_flag)!=0x0000) {
//
//		}
		//printf("Use channel (%d)\n", i);
		ch_cnt++;

		out_offset = zero_pad_num + i*(crop_size+(2*zero_pad_num));
		out_cnt=0;
		for (j=start; j<=end; j++) {
			//printf("[cu] input[%d] -> data[%d]\n", i*input_num+j, out_offset+j-start);
			assert((input_num*12)>i*input_num+j);
			output[out_offset+out_cnt] = input[i*input_num+j];
			//printf("[cu] output[%d]: %f <- input[%d]: %f \n", out_offset+out_cnt, output[out_offset+out_cnt], i*input_num+j, input[i*input_num+j]);
			out_cnt++;
		}
		ch_flag = ch_flag>>1;
	}
}


__device__ float cuCrossCorrelation(float* tmpl_data, int template_size, float* crop_data, float threshold, short indicator)
{
	float pr_corr = 0;
	float tmpl_avg = 0;
	float crop_avg = 0;

	float corr_value = 0;
	float std_tmpl = 0;
	float std_crop = 0;
	float tmp_t = 0;
	float tmp_c = 0;

	int idx=0;
	int corr_loop_cnt = template_size * 12;

	for (idx=0; idx<corr_loop_cnt; idx++) {
		tmpl_avg += tmpl_data[idx];
		crop_avg += crop_data[idx];
	}

	tmpl_avg = tmpl_avg/corr_loop_cnt;
	crop_avg = crop_avg/corr_loop_cnt;

	for (idx=0; idx<corr_loop_cnt; idx++) {
		tmp_t = tmpl_data[idx] - tmpl_avg;
		tmp_c = crop_data[idx] - crop_avg;

		corr_value = corr_value + (tmp_t * tmp_c);
		std_tmpl = std_tmpl + (tmp_t*tmp_t);
		std_crop = std_crop + (tmp_c*tmp_c);
	}

	assert((std_tmpl*std_crop)>0);
	pr_corr = corr_value / (sqrt(std_tmpl*std_crop));

	return pr_corr;
}



__global__ void cuFindMaxIndex(float* dev_tmpl_data, int* dev_tmpl_size, float* dev_thres, short* dev_indicator
		, int* dev_cross_corr_size, float* dev_crop_data, float* dev_cross_corr_out)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int cnt=0;
	int crop_size = *dev_cross_corr_size;
	int template_size = *dev_tmpl_size;
	short indicator = *dev_indicator;
	float cross_corr = 0;
	float threshold = *dev_thres;

//	__shared__ float local_crop[TEMPLATE_SIZE];
	float local_crop[TEMPLATE_SIZE*12];


	//for (idx=0; idx<(crop_size-template_size); idx++) {
		//printf("[%d]th crop from: %d - to : %d\n", idx, idx, (idx+template_size-1));
		cuCropData(local_crop, idx, (idx+template_size-1), dev_crop_data, crop_size, indicator, 0);

		//TempDump("local_crop.bin", local_crop, crop_size*12);

		for (cnt=0; cnt<CU_GAU_WEIGHT_SIZE*12; cnt++) {
			local_crop[cnt] = local_crop[cnt] * cugaussian_weight[cnt%CU_GAU_WEIGHT_SIZE];
//			if (idx==0) {
//				printf("[%d]th cu_local_crop: %f\n", cnt, local_crop[cnt]);
//			}
		}




		cross_corr = cuCrossCorrelation(dev_tmpl_data, template_size, local_crop, threshold, indicator);



//		if (cross_corr>threshold) {
//			printf("[cu] [%d]th cross_corr: %f\n", idx, cross_corr);
//		}

		//printf("[cu] [%d]th cross_corr: %f\n", idx, cross_corr);

	//}


	dev_cross_corr_out[idx] = cross_corr;

}

#endif /* CAPS_CUH_ */
