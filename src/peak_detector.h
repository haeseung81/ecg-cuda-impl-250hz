/*
 * peak_detector.h
 *
 *  Created on: Jan 13, 2015
 *      Author: haeseunl
 */

#ifndef PEAK_DETECTOR_H_
#define PEAK_DETECTOR_H_
#include "common.h"
#include "data_type.h"


int* QRSDetect(int* qrs_data_num, int sampling_rate, float* input, int input_num, int init_sec);
int* QRSDetectVer2(int* qrs_data_num, int sampling_rate, float* input, int input_num, int curr_iter);

void InitBuf(central_data_t* peak_buf, float* input, int sampling_rate, int init_sec);
float UpdateThreshold(central_data_t* peak_buf);
void PeakDetect(central_data_t* peak_buf, float* integral_ecg, int sampling_rate, int input_num, int* peak_num, int** peak_idx_output, float** peak_mag_output);
int FindAllPeak(float* integral_ecg, int input_num, int** all_peak_idx, float** all_peak_mag);
int* CheckPeak(int* qrs_data_num, central_data_t* peak_buf, int sampling_rate, int* peak_idx, float* peak_mag, int peak_idx_num, float* integral_ecg, float threshold);

void AcceptAsQRS(central_data_t* peak_buf, int peak_idx, int* peak_idx_buf, int final_peak_cnt, float peak_mag_input);
void AcceptAsNoise(central_data_t* peak_buf, float noise_input);

int* CheckRange(int* qrs_peak_output, int* qrs_data_num, int template_size);

#endif /* PEAK_DETECTOR_H_ */
