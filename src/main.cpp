/* *
 * Copyright 1993-2012 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 */
#include <stdio.h>
#include <stdlib.h>
#include "common.h"
#include "filter_coeff.h"
#include "file_io.h"
#include "IIR_bandpassfilter.h"

// input parameter
// Model related
//  -m : model window size
// Input data related
//  -t : troushhold value 0.5
//  -s : sampling rate
//  -d : input time interval
//  -c : number of channels
//  -r : sensitivity resolution

#define TEST

//////////////////////
#include "data_type.h"
#include "constants.h"

#include "IIR_bandpassfilter_types.h"
#include "IIR_bandpassfilter_emxAPI.h"
#include "IIR_bandpassfilter_coeff.h"

#include "peak_detector.h"
#include "caps.h"


#include "qrsdet.h"
#include "qrs_c_header.h"

// CUDA Runtime, Interop, and includes
#include <cuda_runtime.h>
#include <vector_types.h>
#include <vector_functions.h>
#include <driver_functions.h>


#define DBG_MAIN_
#ifdef DBG_MAIN
#define PNT_MAIN(...) printf(__VA_ARGS__)
#else
#define PNT_MAIN(...)
#endif

//extern float* DDBuffer;

int fail_cnt = 0;
int time_cnt = 0;

#define SLEEP_
#define TEST

central_data_t peak_buf;
app_setting_t prog_setting;
CLK base_time = 0; // base clk tick

int main(int argc, char **argv) {

	short* init_input;    // initial input
	short** ch_input;    // input data for each channel

	float* filtered_data; // after filtering output
	float** ch_filtered_data; // after filtering output




	int i=0;
	int iter_cnt = 0;
	int max_iter = 0;
	int ch_cnt=0;
	int idx=0;
	int offset_idx = 0;

	int process_data_num = 0;

	float *peak_detect_input = NULL;

	unsigned int start_time_us = 0;
	unsigned int end_time_us = 0;
	unsigned int elapsed_time_us = 0;
	
	float *pTimeFiltering;
	float *pTimePeakDetector;
	float *pTimeCaps;
	float *pTimeValidation;
	
	unsigned int time_us=0;

	// declaration for peak detect
	int qrs_data_num = 0;
	int *qrs_peak_output = NULL;
	float* qrs_init_buf = NULL;
	int template_size = 0;
	int *qrs_peakdata = NULL;

	// declaration for cross-correlation
	short indicator = 0x0FFF;
	int temp = 0;

	// for cross-correlation
	float* iir_output_data = NULL;
	int*	q_idx_buf      = NULL;
	int*	t_idx_buf      = NULL;
	int*	q_idx          = NULL;
	int*	t_idx          = NULL;
	int*	rr_interval;   // peak detection output
	float* q_tmpl_data   = NULL;
	float* t_tmpl_data   = NULL;

	// for QT data verification and analysis
	float* corrected_qt = NULL;
	float avg_corrected_qt = 0;
	float median_corrected_qt = 0;
	float avg_heartrate = 0;
	int   heartrate_num = 0;
	float abnormal_ratio = 0;
	float heartrate_max = 0;
	float heartrate_min = 0;

	int*	cu_q_idx          = NULL;
	int*	cu_t_idx          = NULL;
	int*	cu_rr_interval	  = NULL;


	// filter input/output for 12 channel
	emxArray_real_T** input;
	emxArray_real_T** output;



	// init
	prog_setting.template_size = 0;
	prog_setting.threshold = 0;
	prog_setting.sample_rate = 0;
	prog_setting.input_interval_ms = 0;
	prog_setting.num_channel = 0;
	prog_setting.sensitivity_resolution = 0;

	// input parameter parse
	if (argc!=13) {

#ifndef TEST
		printf("[FATAL] missing parameter!!\n");
		printf(" - usage: %s -c <> -d <> -s <> -t <> -r <> -m <>\n");
		exit(1);
#else
		prog_setting.input_file = "../input/TestECG.bin";
		prog_setting.template_size = TEMPLATE_SIZE;
		prog_setting.threshold = 0.5;
		prog_setting.sample_rate = 250;
		prog_setting.input_interval_ms = 3000;
		prog_setting.num_channel = 12;
		prog_setting.sensitivity_resolution = 0;
#endif

	}
	else {
		// parse input
	}
	max_iter = 3;
	
	pTimeFiltering=(float*)malloc(sizeof(float)*max_iter);
	pTimePeakDetector=(float*)malloc(sizeof(float)*max_iter);
	pTimeCaps=(float*)malloc(sizeof(float)*max_iter);
	pTimeValidation=(float*)malloc(sizeof(float)*max_iter);
	
	for(int n=0;n<<max_iter;n++){
		pTimeFiltering[n]=0;
		pTimePeakDetector[n]=0;
		pTimeCaps[n]=0;
		pTimeValidation[n]=0;
	}
	// initialization of peak buffer index
	peak_buf.sig_next_idx   = 0;
	peak_buf.noise_next_idx = 0;
	peak_buf.rr_next_idx    = 0;

	// read model data
	int input_num = -1;
	init_input = ReadEcgInput(prog_setting.input_file, prog_setting.num_channel, &input_num);
	assert(input_num>0);

	// set base clock
	base_time = GetBaseTimeUs();

	// memory allocation for qrs detect
//	DDBuffer = NULL;
//	DDBuffer = (int*)malloc(sizeof(int)*DER_DELAY);


	// filter input/output data allocation
	input = (emxArray_real_T**)malloc(sizeof(emxArray_real_T*)*prog_setting.num_channel);
	output = (emxArray_real_T**)malloc(sizeof(emxArray_real_T*)*prog_setting.num_channel);

	process_data_num = (prog_setting.sample_rate * prog_setting.input_interval_ms)/1000;
	for (ch_cnt=0; ch_cnt<prog_setting.num_channel; ch_cnt++) {
		input[ch_cnt] = emxCreate_real_T(1, process_data_num);
		output[ch_cnt] = emxCreate_real_T(1, process_data_num);
	}

	// offset for channel   : input_num * ch_cnt;
	// offset for each 3 sec: iter_cnt * process_data_num;
	// total offset : (input_num * ch_cnt) + (iter_cnt * process_data_num)


	///////////////////////////////////////////////////////////////////////////////////////
	// 0. Model extraction - put data from model file.

//	q_tmpl_data = (float*)malloc(sizeof(float)*prog_setting.num_channel*prog_setting.template_size);
//	t_tmpl_data = (float*)malloc(sizeof(float)*prog_setting.num_channel*prog_setting.template_size);

	q_tmpl_data = ReadModelInput("../input/q_template.bin", prog_setting.num_channel, &temp);
	t_tmpl_data = ReadModelInput("../input/t_template.bin", prog_setting.num_channel, &temp);

	// apply gaussian weight mask
	for (idx=0; idx<GAU_WEIGHT_SIZE*prog_setting.num_channel; idx++) {
		q_tmpl_data[idx] = q_tmpl_data[idx] * gaussian_weight[idx%GAU_WEIGHT_SIZE];
		t_tmpl_data[idx] = t_tmpl_data[idx] * gaussian_weight[idx%GAU_WEIGHT_SIZE];
	}
	// initialize QRS peak detector C function (for Ver2)
	QRSDet( 0, 1);
	///////////////////////////////////////////////////////////////////////////////////////



	// memory allocation for peak detect input
	peak_detect_input = (float*)malloc(sizeof(float)*process_data_num);
	iter_cnt = 0;

	// wait 5 sec for settle down
	usleep(5000000);



	///////////////////////////////////////////////////////////////////////////////////////////////
	// main body for processing data for 3 sec



	while (iter_cnt<max_iter) {

		PNT_MAIN("=====================================================================\n");
		PNT_MAIN(" Time range: %d - %d sec (iter_cnt: %d | max_iter: %d)\n", iter_cnt*(prog_setting.input_interval_ms/1000), (iter_cnt+1)*(prog_setting.input_interval_ms/1000), iter_cnt, max_iter);
		PNT_MAIN("---------------------------------------------------------------------\n");

		time_cnt = iter_cnt;

		start_time_us = GetTimeUs(base_time);
		// processing main body
		time_us = GetTimeUs(base_time);
		///////////////////////////////////////////////////////////////////////////////////
		// 1. filter
		for (ch_cnt=0; ch_cnt<prog_setting.num_channel; ch_cnt++) {

			//printf("(%d)th channel filter start\n", ch_cnt);

			// fetch 3 second input data from the original data
			offset_idx = (input_num * ch_cnt) + (iter_cnt * process_data_num);
			for(idx=0; idx<process_data_num; idx++) {
				input[ch_cnt]->data[idx] = (double)init_input[offset_idx+idx];
			}

			IIR_bandpassfilter(input[ch_cnt], output[ch_cnt], first_coeff_a, first_coeff_b, first_coeff_c);

			//printf("(%d)th channel filter complete\n", ch_cnt);
		}

		// extract second channel output
		for (idx = 0; idx<process_data_num; idx++){
			peak_detect_input[idx] = output[7]->data[idx];
			//printf("peak_detect_input[%d]: %f\n", idx, peak_detect_input[idx]);
		}
		pTimeFiltering[iter_cnt] = (float)(GetTimeUs(base_time) - time_us)/1000;
//		TempDump("peak_detect_input.bin", peak_detect_input, process_data_num);


		///////////////////////////////////////////////////////////////////////////////////

		///////////////////////////////////////////////////////////////////////////////////////
		// 2. peak detect - use channel II only
		time_us=GetTimeUs(base_time);
		qrs_data_num = 0;

		// Main body for peak detector
		qrs_peakdata = QRSDetectVer2(&qrs_data_num, prog_setting.sample_rate, peak_detect_input, process_data_num, iter_cnt);



		PNT_MAIN("# of peak: %d\n", qrs_data_num);
		//printf("[MAIN : Peak Detection] found peaks \n");
		//for(int n=0;n<qrs_data_num;n++){
		//	printf("%d ",qrs_peakdata[n]);
	//	}
	//	printf("\n");	
		pTimePeakDetector[iter_cnt]=(float)(GetTimeUs(base_time)-time_us)/1000;
		if (qrs_data_num>0) {
			template_size = prog_setting.template_size;
			
			///////////////////////////////////////////////////////////////////////////////////////


			///////////////////////////////////////////////////////////////////////////////////////
			// 3. Compute cross-correlation and finalize Q point, T point, RR interval
			// merge iir filter output into 1-d array
			time_us=GetTimeUs(base_time);
			iir_output_data = (float*)malloc(sizeof(float)*process_data_num*prog_setting.num_channel);

			i=0;
			for (ch_cnt=0; ch_cnt<prog_setting.num_channel; ch_cnt++) {
				for (idx=0; idx<process_data_num; idx++) {
					iir_output_data[i] = output[ch_cnt]->data[idx];
					i++;
				}
			}

			//TempDump("iir_output_data.bin", iir_output_data, process_data_num*12);

//			TempDump("q_tmpl.bin", q_tmpl_data, template_size*12);
//			TempDump("t_tmpl.bin", t_tmpl_data, template_size*12);

			FindMatchedQT(q_tmpl_data, t_tmpl_data, template_size, qrs_peakdata, qrs_data_num,
					iir_output_data, process_data_num, indicator, &q_idx, &t_idx, &rr_interval, iter_cnt);



			pTimeCaps[iter_cnt]=(float)(GetTimeUs(base_time)-time_us)/1000;
			///////////////////////////////////////////////////////////////////////////////////////



			///////////////////////////////////////////////////////////////////////////////////////
			cuFindMatchedQT(q_tmpl_data, t_tmpl_data, template_size, qrs_peakdata, qrs_data_num
					, iir_output_data, process_data_num, indicator, &cu_q_idx, &cu_t_idx, &cu_rr_interval, iter_cnt);
			///////////////////////////////////////////////////////////////////////////////////////


			///////////////////////////////////////////////////////////////////////////////////////
			// 4. Align the found QT data.
			// merge iir filter output into 1-d array
			//RefineFindedQT(qrs_peakdata, qrs_data_num, q_idx_buf, t_idx_buf, &q_idx, &t_idx, &rr_interval);


			///////////////////////////////////////////////////////////////////////////////////////


			///////////////////////////////////////////////////////////////////////////////////////
			// 5. Data validation
			// merge iir filter output into 1-d array
			time_us=GetTimeUs(base_time);
			QTdataVerification(q_idx, t_idx, rr_interval, (qrs_data_num-2), prog_setting.sample_rate,
					&corrected_qt, &avg_corrected_qt, &median_corrected_qt,
					&avg_heartrate, &heartrate_num, &abnormal_ratio, &heartrate_max, &heartrate_min);
			pTimeValidation[iter_cnt]=(float)(GetTimeUs(base_time)-time_us)/1000;
			///////////////////////////////////////////////////////////////////////////////////////
		}



#ifndef TEST
#endif

		// complete the processing
		elapsed_time_us = GetTimeUs(base_time) - start_time_us;
		iter_cnt++;
		PNT_MAIN("elapsed_time_us: %u\n", elapsed_time_us);

		PNT_MAIN("=====================================================================\n\n");
		//free(qrs_peak_output);

		// for now, sleep 1 sec
		usleep(1000000);

		// compute sleep time
#ifdef SLEEP
		usleep((3000000 - elapsed_time_us));
#endif
	}

	printf("max_iter: %d | fail_cnt: %d\n", max_iter, fail_cnt);
	TempDump("pTimeFiltering.bin",pTimeFiltering,max_iter);
	TempDump("pTimePeakDetector.bin",pTimePeakDetector,max_iter);
	TempDump("pTimeCaps.bin",pTimeCaps,max_iter);
	TempDump("pTimeValidation.bin",pTimeValidation,max_iter);
	//free(DDBuffer);



	return 0;
}


