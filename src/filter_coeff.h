/*
 * filter_coeff.h
 *
 *  Created on: Jan 8, 2015
 *      Author: haeseunl
 */

#ifndef FILTER_COEFF_H_
#define FILTER_COEFF_H_

float wide_coeff_b[] = { 0.0215, 0, -0.1075, 0, 0.2151, 0, -0.2151, 0, 0.1075, 0, -0.0215 };
float wide_coeff_a[] = { 1.0000, -5.9777, 15.8766, -25.0943, 26.6697, -20.1723, 10.9605, -4.1746, 1.0713, -0.1708, 0.0116};

float wide_low_coeff_b[] = { 0.0219,  0.1097, 0.2194,  0.2194, 0.1097,  0.0219};
float wide_low_coeff_a[] = { 1.0000, -0.9853, 0.9738, -0.3864, 0.1112, -0.0113};

//float wide_high_coeff_b[] = { 0.98988508, -4.94942538, 9.89885075, -9.89885075, 4.94942538, -0.98988508 };
//float wide_high_coeff_a[] = { 1.0000    , -4.97966719, 9.91887534, -9.87862155, 4.91928587, -0.97987246 };
//float wide_high_coeff_b[] = { -0.0003, -0.0012, -0.0031, 0.9957, -0.0031, -0.0012, -0.0003 };
float wide_high_coeff_b[] = { -0.0019, -0.0074, -0.0184, 0.9741, -0.0184, -0.0074, -0.0019 };
float wide_high_coeff_a[] = { 1, 0, 0, 0, 0, 0, 0 };

#define SCALE 0.001

float narr_coeff_b[] = {
	    0.0214*SCALE,
	         0*SCALE,
	   -0.1070*SCALE,
	         0*SCALE,
	    0.2140*SCALE,
	         0*SCALE,
	   -0.2140*SCALE,
	         0*SCALE,
	    0.1070*SCALE,
	         0*SCALE,
	   -0.0214*SCALE
};

float narr_coeff_a[] = {1.0000, -8.9694, 36.4210, -88.1673, 140.9106, -155.3594, 119.6712, -63.5941, 22.3132, -4.6681, 0.4422};


#endif /* FILTER_COEFF_H_ */
