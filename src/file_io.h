/*
 * file_io.h
 *
 *  Created on: Jan 7, 2015
 *      Author: haeseunl
 */

#ifndef FILE_IO_H_
#define FILE_IO_H_

short* ReadEcgInput(char *input_filename, int channel_num, int* data_num_per_ch);
float* ReadModelInput(char *input_filename, int channel_num, int* data_num_per_ch);
void TempDump(char* filename, float* data, int data_num);

#endif /* FILE_IO_H_ */
