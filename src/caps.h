/*
 * caps.h
 *
 *  Created on: Jan 20, 2015
 *      Author: haeseunl
 */

#ifndef CAPS_H_
#define CAPS_H_

#include "common.h"
#include "data_type.h"
#include "file_io.h"

#define GAU_WEIGHT_SIZE		50
extern float gaussian_weight[];

void FindMatchedQT(float* q_tmpl_data, float* t_tmpl_data , int template_size
		, int* qrs_peakdata, int qrs_data_num
		, float* iir_output_data, int input_num, short indicator
		, int** q_idx_out, int** t_idx_out, int** rr_interval, int iter_cnt);

void RefineFindedQT(int* qrs_peakdata, int qrs_data_num, int* q_idx_buf, int* t_idx_buf, int** q_idx, int** t_idx, int** rr_interval);


int FindMaxIndex(float* tmpl_data, int template_size, float* crop_data, int crop_size, float threshold, short indicator);
float CrossCorrelation(float* tmpl_data, int template_size, float* crop_data, float threshold, short indicator);

void QTdataVerification(int* q_idx, int* t_idx, int* rr_interval, int idx_num, int sampling_rate
		, float** corrected_qt, float* avg_corrected_qt, float* median_corrected_qt
		, float* avg_heartrate, int* heartrate_num, float* abnormal_ratio, float* heartrate_max, float* heartrate_min);

extern "C"
void cuFindMatchedQT(float* q_tmpl_data, float* t_tmpl_data , int template_size
		, int* qrs_peakdata, int qrs_data_num
		, float* iir_output_data, int input_num, short indicator
		, int** q_idx_out, int** t_idx_out, int** rr_interval, int iter_cnt);

#endif /* CAPS_H_ */
