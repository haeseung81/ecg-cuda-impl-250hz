/*
 * IIR_bandpassfilter.c
 *
 * Code generation for function 'IIR_bandpassfilter'
 *
 * C source code generated on: Mon Jan 12 12:33:15 2015
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "IIR_bandpassfilter.h"
#include "IIR_bandpassfilter_emxutil.h"
#include "filter.h"
#include <stdio.h>

/*
real_T first_coeff_a[11] = { 1.0, -1.9864100375624976, -1.1045965530105768,
			3.1803012204716605, 1.5015119943422237, -2.9023550769152804,
			-1.2223848982942684, 1.2978851964386844, 0.59478413479107517,
			-0.23862570844921471, -0.12011026830057617 };

real_T first_coeff_b[11] = { 0.34657209275210371, 0.0, -1.7328604637605187,
			0.0, 3.4657209275210374, 0.0, -3.4657209275210374, 0.0, 1.7328604637605187,
			0.0, -0.34657209275210371 };

real_T first_coeff_c[10] = { -0.34657203113943247, -0.34657215352746107,
  1.3862882421759137, 1.3862884381227671, -2.0794323968861055,
  -2.0794325757079553, 1.3862882764986835, 1.3862883564648576,
  -0.34657207064942197, -0.34657208535178929 };

 */
real_T first_coeff_b[11] = { 0.021508969857543258, 0.0,
		-0.10754484928771629, 0.0, 0.21508969857543259, 0.0, -0.21508969857543259,
		0.0, 0.10754484928771629, 0.0, -0.021508969857543258 };

real_T first_coeff_a[11] = { 1.0, -5.9777185791307463, 15.87661413796296,
		-25.094270878644526, 26.669655590470963, -20.172286140791449,
		10.960497088333339, -4.1746403477516152, 1.0713284267871743,
		-0.17077673997007742, 0.011597442740865254 };
real_T first_coeff_c[10] = { -0.021521067205938115, -0.021448752661679997,
		0.0859040316934786, 0.08620760583101246, -0.12920472485966875,
		-0.12896069368630284, 0.08599641193727138, 0.086046914015981343,
		-0.021510895504959091, -0.021508829559237934 };


real_T second_coeff_a[11] = { 1.0, -8.7166425462247759, 34.490079410154422,
		-81.577597119418783, 127.72898843621377, -138.33310145473092,
		104.95049678679376, -55.079365541316974, 19.137981288612963,
		-3.9759312599004484, 0.375092913433051 };

real_T second_coeff_b[11] = { 4.9610354332612047E-5, 0.0,
		-0.00024805177166306022, 0.0, 0.00049610354332612044, 0.0,
		-0.00049610354332612044, 0.0, 0.00024805177166306022, 0.0,
		-4.9610354332612047E-5 };


real_T second_coeff_c[10] = { -4.9610352753041134E-5, -4.96103665215962E-5,
		0.00019844145962099035, 0.00019844133076339093, -0.000297662010805735,
		-0.00029766222931267884, 0.00019844147979019292, 0.00019844139278842919,
		-4.9610348644832542E-5, -4.9610354925097905E-5 };


/* Function Definitions */
void IIR_bandpassfilter(emxArray_real_T *input, emxArray_real_T *output, real_T* coeff_a, real_T* coeff_b, real_T* coeff_c)
{
	int32_T md2;
	int32_T i0;
	emxArray_real_T *y;
	real_T xtmp;
	real_T b_y;
	int32_T loop_ub;
	real_T a[10];
	//  static const real_T b_a[10] = { -0.34657203113943247, -0.34657215352746107,
	//    1.3862882421759137, 1.3862884381227671, -2.0794323968861055,
	//    -2.0794325757079553, 1.3862882764986835, 1.3862883564648576,
	//    -0.34657207064942197, -0.34657208535178929 };

	real_T* b_a = coeff_c;

	emxArray_real_T *c_y; // zero-padded input
	int32_T m;
	emxArray_real_T *d_y;
	emxArray_int32_T *r0;

	/* Nyq = Fs/2; */
	/* Wn = zeros(1,2); */
	/* Wn = [0.5, 100]/Nyq; */
	md2 = input->size[1];
	if (md2 == 0) {
		i0 = output->size[0] * output->size[1];
		output->size[0] = 0;
		output->size[1] = 0;
		emxEnsureCapacity((emxArray__common *)output, i0, (int32_T)sizeof(real_T));
	} else {
		emxInit_real_T(&y, 1);            // emxArray_real_T buffer - 1 dimension
		xtmp = 2.0 * input->data[0];      // input[0] * 2.0
		md2 = input->size[1];             // input length
		b_y = 2.0 * input->data[md2 - 1]; // input[last] * 2.0
		md2 = input->size[1];             // input length
		i0 = y->size[0];
		y->size[0] = 60 + input->size[1];
		emxEnsureCapacity((emxArray__common *)y, i0, (int32_T)sizeof(real_T));
		for (i0 = 0; i0 < 30; i0++) {
			y->data[i0] = xtmp - input->data[30 - i0];
		}

		loop_ub = input->size[1];
		for (i0 = 0; i0 < loop_ub; i0++) {
			y->data[i0 + 30] = input->data[i0];
		}

		for (i0 = 0; i0 < 30; i0++) {
			y->data[(i0 + input->size[1]) + 30] = b_y - input->data[(md2 - i0) - 2];
		}

		xtmp = y->data[0];               // input[30]*2 - input[30]
		//printf("---------- 1st filter ----------\n");
		//printf("xtmp: %f\n", xtmp);
		for (i0 = 0; i0 < 10; i0++) {
			a[i0] = b_a[i0] * xtmp;

			//printf("b_a[%d]: %f | a[%d]: %f\n", i0, b_a[i0], i0, a[i0]);
		}

		emxInit_real_T(&c_y, 1);
		i0 = c_y->size[0];
		c_y->size[0] = y->size[0];
		emxEnsureCapacity((emxArray__common *)c_y, i0, (int32_T)sizeof(real_T));
		loop_ub = y->size[0];
		for (i0 = 0; i0 < loop_ub; i0++) {
			c_y->data[i0] = y->data[i0];
		}

		//  filter(c_y, a, y);
		filter(c_y, a, y, coeff_a, coeff_b);
		m = y->size[0];
		i0 = y->size[0];
		md2 = i0 / 2;
		loop_ub = 1;
		emxFree_real_T(&c_y);
		while (loop_ub <= md2) {
			xtmp = y->data[loop_ub - 1];
			y->data[loop_ub - 1] = y->data[m - loop_ub];
			y->data[m - loop_ub] = xtmp;
			loop_ub++;
		}

		xtmp = y->data[0];


		//printf("---------- 2nd filter ----------\n");
		//printf("xtmp: %f\n", xtmp);
		for (i0 = 0; i0 < 10; i0++) {
			a[i0] = b_a[i0] * xtmp;
		}


		emxInit_real_T(&d_y, 1);
		i0 = d_y->size[0];
		d_y->size[0] = y->size[0];
		emxEnsureCapacity((emxArray__common *)d_y, i0, (int32_T)sizeof(real_T));
		loop_ub = y->size[0];
		for (i0 = 0; i0 < loop_ub; i0++) {
			d_y->data[i0] = y->data[i0];
		}

		//filter(d_y, a, y);
		filter(d_y, a, y, coeff_a, coeff_b);
		m = y->size[0];
		i0 = y->size[0];
		md2 = i0 / 2;
		loop_ub = 1;
		emxFree_real_T(&d_y);
		while (loop_ub <= md2) {
			xtmp = y->data[loop_ub - 1];
			y->data[loop_ub - 1] = y->data[m - loop_ub];
			y->data[m - loop_ub] = xtmp;
			loop_ub++;
		}

		emxInit_int32_T(&r0, 1);
		md2 = input->size[1];
		loop_ub = (int32_T)((real_T)md2 + 30.0) - 31;
		i0 = r0->size[0];
		r0->size[0] = (int32_T)((real_T)md2 + 30.0) - 30;
		emxEnsureCapacity((emxArray__common *)r0, i0, (int32_T)sizeof(int32_T));
		for (i0 = 0; i0 <= loop_ub; i0++) {
			r0->data[i0] = 31 + i0;
		}

		i0 = output->size[0] * output->size[1];
		output->size[0] = 1;
		emxEnsureCapacity((emxArray__common *)output, i0, (int32_T)sizeof(real_T));
		md2 = r0->size[0];
		i0 = output->size[0] * output->size[1];
		output->size[1] = md2;
		emxEnsureCapacity((emxArray__common *)output, i0, (int32_T)sizeof(real_T));
		loop_ub = r0->size[0];
		for (i0 = 0; i0 < loop_ub; i0++) {
			output->data[i0] = y->data[r0->data[i0] - 1];
		}

		emxFree_int32_T(&r0);
		emxFree_real_T(&y);
	}
}

/* End of code generation (IIR_bandpassfilter.c) */
