/*
 * IIR_bandpassfilter_initialize.c
 *
 * Code generation for function 'IIR_bandpassfilter_initialize'
 *
 * C source code generated on: Mon Jan 12 12:33:14 2015
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "IIR_bandpassfilter.h"
#include "IIR_bandpassfilter_initialize.h"

/* Function Definitions */
void IIR_bandpassfilter_initialize(void)
{
  rt_InitInfAndNaN(8U);
}

/* End of code generation (IIR_bandpassfilter_initialize.c) */
