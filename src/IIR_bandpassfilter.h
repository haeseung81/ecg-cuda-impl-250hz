/*
 * IIR_bandpassfilter.h
 *
 * Code generation for function 'IIR_bandpassfilter'
 *
 * C source code generated on: Mon Jan 12 12:33:15 2015
 *
 */

#ifndef __IIR_BANDPASSFILTER_H__
#define __IIR_BANDPASSFILTER_H__
/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "rtwtypes.h"
#include "IIR_bandpassfilter_types.h"

//real_T coeff_a[11] = { 1.0, -1.9864100375624976, -1.1045965530105768,
//			3.1803012204716605, 1.5015119943422237, -2.9023550769152804,
//			-1.2223848982942684, 1.2978851964386844, 0.59478413479107517,
//			-0.23862570844921471, -0.12011026830057617 };
//
//real_T coeff_b[11] = { 0.34657209275210371, 0.0, -1.7328604637605187,
//			0.0, 3.4657209275210374, 0.0, -3.4657209275210374, 0.0, 1.7328604637605187,
//			0.0, -0.34657209275210371 };
//
//real_T coeff_c[10] = { -0.34657203113943247, -0.34657215352746107,
//  1.3862882421759137, 1.3862884381227671, -2.0794323968861055,
//  -2.0794325757079553, 1.3862882764986835, 1.3862883564648576,
//  -0.34657207064942197, -0.34657208535178929 };


/* Function Declarations */
//extern void IIR_bandpassfilter(emxArray_real_T *input, emxArray_real_T *output);
extern void IIR_bandpassfilter(emxArray_real_T *input, emxArray_real_T *output, real_T* coeff_a, real_T* coeff_b, real_T* coeff_c);
#endif
/* End of code generation (IIR_bandpassfilter.h) */
