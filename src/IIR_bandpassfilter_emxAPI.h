/*
 * IIR_bandpassfilter_emxAPI.h
 *
 * Code generation for function 'IIR_bandpassfilter_emxAPI'
 *
 * C source code generated on: Mon Jan 12 12:33:15 2015
 *
 */

#ifndef __IIR_BANDPASSFILTER_EMXAPI_H__
#define __IIR_BANDPASSFILTER_EMXAPI_H__
/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "rtwtypes.h"
#include "IIR_bandpassfilter_types.h"

/* Function Declarations */
extern emxArray_real_T *emxCreateND_real_T(int32_T numDimensions, int32_T *size);
extern emxArray_real_T *emxCreateWrapperND_real_T(real_T *data, int32_T numDimensions, int32_T *size);
extern emxArray_real_T *emxCreateWrapper_real_T(real_T *data, int32_T rows, int32_T cols);
extern emxArray_real_T *emxCreate_real_T(int32_T rows, int32_T cols);
extern void emxDestroyArray_real_T(emxArray_real_T *emxArray);
#endif
/* End of code generation (IIR_bandpassfilter_emxAPI.h) */
