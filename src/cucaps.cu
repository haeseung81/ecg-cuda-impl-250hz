#include "caps.h"
#include "cucaps.cuh"

#define DBG_CUFIND_MATCH
#ifdef DBG_CUFIND_MATCH
#define PNT_CUFIND_MATCH(...) printf(__VA_ARGS__)
#else
#define PNT_CUFIND_MATCH(...)
#endif

extern CLK base_time;
extern "C"
void cuFindMatchedQT(float* q_tmpl_data, float* t_tmpl_data , int template_size
		, int* qrs_peakdata, int qrs_data_num
		, float* iir_output_data, int input_num, short indicator
		, int** q_idx_out, int** t_idx_out, int** rr_interval, int iter_cnt)
{
	//int ch_num = 0;
	int idx=0;
	int cnt=0;
	int start = 0;
	int end = 0;
	float* local_data = NULL;
	int max_data_num = 0;
	int q_idx = 0;
	int t_idx = 0;
	int cross_corr_size = 0;



	// template memory allocation;
	(*q_idx_out) = (int*)malloc(sizeof(int)*(qrs_data_num-1));
	(*t_idx_out) = (int*)malloc(sizeof(int)*(qrs_data_num-1));
	(*rr_interval) = (int*)malloc(sizeof(int)*(qrs_data_num-1));

	//
	//	(*template_data) = CropMemAlloc((end-start+1), (template_size/2));
	//	CropData(local_crop_data, start, end, iir_output_data, input_num, indicator, (template_size/2));
	//	local_crop_data = (*template_data);

	// find max memory size for memory allocation
	for(cnt=0; cnt<(qrs_data_num-1); cnt++) {
		if (max_data_num < (qrs_peakdata[cnt+1]-qrs_peakdata[cnt]+1)) {
			max_data_num = (qrs_peakdata[cnt+1]-qrs_peakdata[cnt]+1);
		}
	}

	// memory allocation with maximum data number
	//PNT_FIND_MATCH("max_data_num: %d\n", max_data_num);
	local_data = CropMemAlloc(max_data_num, (template_size/2));

	// Memory allocation for CUDA
	float* dev_q_tmpl_data;
	float* dev_t_tmpl_data;
	int* dev_tmpl_size;
	float* dev_thres;
	short* dev_indicator; // 0: q | 1: t
	int* dev_crop_size;
	float* dev_local_data;

	int* dev_cross_corr_size;
	float* dev_cross_corr_out;
	float* local_crop;
	float* cross_corr_out;
	float max_cross_corr = -1;
	int max_idx = 0;
	int data_idx = 0;

	// Allocate memory for CUDA
	cudaMalloc((void **) &dev_q_tmpl_data, template_size*12*sizeof(float));
	cudaMalloc((void **) &dev_t_tmpl_data, template_size*12*sizeof(float));
	cudaMalloc((void **) &dev_tmpl_size, sizeof(int));
	cudaMalloc((void **) &dev_thres, sizeof(float));
	cudaMalloc((void **) &dev_indicator, sizeof(short));

	cudaMalloc((void **) &dev_crop_size, sizeof(int));
	cudaMalloc((void **) &dev_cross_corr_size, sizeof(int));
	cudaMalloc((void **) &dev_cross_corr_out, (max_data_num+((template_size/2)*2))*sizeof(float));
	cross_corr_out = (float*)malloc((max_data_num+((template_size/2)*2))*sizeof(float));





	//======================================================
	// Data processing with CUDA
	// - Input:
	// - Output: Crosss correlation value for each index
	// 

	// Fineing Q and T index from single beat
	// PNT_FIND_MATCH("-------- Find Q --------\n");
	//q_idx = FindMaxIndex(q_tmpl_data, template_size, local_data, cross_corr_size, prog_setting.threshold, indicator);

	//PNT_FIND_MATCH("-------- Find T --------\n");
	//t_idx = FindMaxIndex(t_tmpl_data, template_size, local_data, cross_corr_size, prog_setting.threshold, indicator);

	int max_size=0;

	for(cnt=0; cnt<(qrs_data_num-1); cnt++) {
		start = qrs_peakdata[cnt];
		end = qrs_peakdata[cnt+1];

		if ((end-start+1)>max_size) {
			max_size = (end-start+1);
		}
	}


	cudaMalloc((void **) &dev_local_data, (max_size+template_size)*12*sizeof(float));


	////////////////////// CUDA Timing /////////////////
	cudaEvent_t time_start, time_stop;
	cudaEventCreate(&time_start);
	cudaEventCreate(&time_stop);


	for(cnt=0; cnt<(qrs_data_num-1); cnt++) {
		start = qrs_peakdata[cnt];
		end = qrs_peakdata[cnt+1];
		PNT_CUFIND_MATCH("----------------------------------------------------------\n");
		PNT_CUFIND_MATCH("[%d]th beat - start: %d | end; %d | size: %d : input_num: %d\n", cnt+1, start, end , (end-start+1), input_num);

		//printf("[CropData]\n");
		//TempDump("iir_output_data.bin", iir_output_data, input_num*12);

		assert((start-(iter_cnt*input_num))>=0);
		assert((end-(iter_cnt*input_num))>=0);

		if((start-(iter_cnt*input_num))>(template_size/2)){
			if((end-(iter_cnt*input_num)+template_size/2)<input_num){
				CropData(local_data,(start-(iter_cnt*input_num)-template_size/2), (end-(iter_cnt*input_num))+template_size/2, iir_output_data, input_num, indicator, 0);	

			}
		}
		else { 
			CropData(local_data, (start-(iter_cnt*input_num)), (end-(iter_cnt*input_num)), iir_output_data, input_num, indicator, (template_size/2));
		}

		cross_corr_size = (end-start+1) + template_size;

		//TempDump("cropdata.bin", local_data, cross_corr_size*12);
		// Data transfer from the Host to Device
		cudaMemcpy(dev_q_tmpl_data, q_tmpl_data, template_size*12*sizeof(float), cudaMemcpyHostToDevice);
		cudaMemcpy(dev_t_tmpl_data, t_tmpl_data, template_size*12*sizeof(float), cudaMemcpyHostToDevice);
		cudaMemcpy(dev_tmpl_size, &template_size, sizeof(int), cudaMemcpyHostToDevice);
		cudaMemcpy(dev_thres, &prog_setting.threshold, sizeof(float), cudaMemcpyHostToDevice);
		cudaMemcpy(dev_indicator, &indicator, sizeof(short), cudaMemcpyHostToDevice);
		cudaMemcpy(dev_cross_corr_size, &cross_corr_size, sizeof(int), cudaMemcpyHostToDevice);
		cudaMemcpy(dev_local_data, local_data, (cross_corr_size*12)*sizeof(float), cudaMemcpyHostToDevice);
		

		CLK start_time_us = GetTimeUs(base_time);
		// Fineing Q and T index from single beat
		PNT_CUFIND_MATCH("-------- [CUDA] Find Q --------\n");
		//q_idx = FindMaxIndex(q_tmpl_data, template_size, local_data, cross_corr_size, prog_setting.threshold, indicator);
		//cuFindMaxIndex<<<1, 3>>>(dev_q_tmpl_data
		//CLK start_time_us=GetTimeUs(base_time);

		cuFindMaxIndex<<<2, ((cross_corr_size-template_size))/2>>>(dev_q_tmpl_data
				, dev_tmpl_size
				, dev_thres
				, dev_indicator
				, dev_cross_corr_size
				, dev_local_data
				, dev_cross_corr_out);
		cudaDeviceSynchronize();
		//CLK elapsed_time_us = GetTimeUs(base_time) - start_time_us;

		cudaMemcpy(cross_corr_out, dev_cross_corr_out, (cross_corr_size-template_size)*sizeof(float), cudaMemcpyDeviceToHost);
		cudaDeviceSynchronize();

		for (int i=0; i<(cross_corr_size-template_size); i++) {
			if (max_cross_corr<cross_corr_out[i]) {
				max_cross_corr = cross_corr_out[i];
				max_idx = i;
			}
		}


		if (max_cross_corr>prog_setting.threshold) {
			data_idx = max_idx;
			//printf("[CUDA] Found max index: %d (%f)\n", data_idx, max_cross_corr);
		}
		else {
			data_idx = -1;
		}

		if (data_idx>-1) {
			q_idx = data_idx + start-1;
		}



		PNT_CUFIND_MATCH("-------- [CUDA] Find T --------\n");
		//q_idx = FindMaxIndex(q_tmpl_data, template_size, local_data, cross_corr_size, prog_setting.threshold, indicator);
		//cuFindMaxIndex<<<1, 3>>>(dev_q_tmpl_data
		//CLK start_time_us=GetTimeUs(base_time);
		cuFindMaxIndex<<<2, ((cross_corr_size-template_size))/2>>>(dev_t_tmpl_data
				, dev_tmpl_size
				, dev_thres
				, dev_indicator
				, dev_cross_corr_size
				, dev_local_data
				, dev_cross_corr_out);
		cudaDeviceSynchronize();
		//CLK elapsed_time_us = GetTimeUs(base_time) - start_time_us;
		cudaMemcpy(cross_corr_out, dev_cross_corr_out, (cross_corr_size-template_size)*sizeof(float), cudaMemcpyDeviceToHost);
		cudaDeviceSynchronize();

		CLK elapsed_time_us = GetTimeUs(base_time) - start_time_us;

		printf("[CUDA] elapsed_time_us: %u\n", elapsed_time_us);

		max_cross_corr = -1;
		for (int i=0; i<(cross_corr_size-template_size); i++) {
			if (max_cross_corr<cross_corr_out[i]) {
				max_cross_corr = cross_corr_out[i];
				max_idx = i;
			}
		}


		if (max_cross_corr>prog_setting.threshold) {
			data_idx = max_idx;
			printf("[CUDA] Found max index: %d (%f)\n", data_idx, max_cross_corr);
		}
		else {
			data_idx = -1;
		}


		if (data_idx>-1) { t_idx = data_idx + start-1; }


		(*q_idx_out)[cnt] = q_idx;
		(*t_idx_out)[cnt] = t_idx;

		PNT_CUFIND_MATCH("[CUDA] >>> [%d]th beat - q_idx: %d | t_idx: %d\n\n", cnt+1, q_idx, t_idx);


		// record final Q and T index

		if (cnt<(qrs_data_num-2)) {
			assert(cnt<(qrs_data_num-2));
			(*q_idx_out)[cnt] = q_idx;
			PNT_CUFIND_MATCH("[CUDA] >>> [%d]th beat - q_idx: %d\n", cnt, q_idx);
		}
		if (cnt>0) {
			assert(cnt>0);
			(*t_idx_out)[cnt-1] = t_idx;
			PNT_CUFIND_MATCH("[CUDA] >>> [%d]th beat - t_idx: %d\n", cnt, t_idx);
		}

		// RR interval data
		if (cnt<(qrs_data_num-2)) {
			(*rr_interval)[cnt] = qrs_peakdata[cnt+2] - qrs_peakdata[cnt+1];
			PNT_CUFIND_MATCH(" >>>[CUDA] [%d]th beat - rr_interval: %d\n", cnt, (*rr_interval)[cnt]);
		}

		PNT_CUFIND_MATCH(" >>>[CUDA] [%d]qrs_peakdata: %d\n", cnt, qrs_peakdata[cnt]);


	}
}





