/*
 * qrs_c-header.h
 *
 *  Created on: Feb 5, 2015
 *      Author: haeseunl
 */

#ifndef QRS_C_HEADER_H_
#define QRS_C_HEADER_H_

float QRSDet( float datum, int init);
float QRSFilter(float datum, int init) ;
float deriv1( float x0, int init ) ;

float lpfilt( float datum ,int init) ;
float hpfilt( float datum, int init ) ;
float deriv1( float x0, int init ) ;
float deriv2( float x0, int init ) ;
float mvwint(float datum, int init) ;

#endif /* QRS_C_HEADER_H_ */
