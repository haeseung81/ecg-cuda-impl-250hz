#include "common.h"
#include "file_io.h"

#define DEBUG_

short* ReadEcgInput(char *input_filename, int channel_num, int* data_num_per_ch)
{
	FILE* file_in=NULL;
	char * line = NULL;
	int file_size;
	size_t len = 0;
	ssize_t read;
	short *input_data;
	int i = 0;

	file_in=fopen(input_filename, "r");

	if (file_in==NULL) {
		printf("[FATAL] Fail to open file [%s]\n", input_filename);
		exit(1);
	}
	else {
		fseek (file_in , 0 , SEEK_END);
		file_size = ftell (file_in);
		rewind (file_in);
		*data_num_per_ch = file_size/sizeof(short)/channel_num;
		printf("Input file size       : (%d)bytes \n", file_size);
		printf("Number of input data  : %d\n", file_size/sizeof(short));
		printf("Number of data/channel: %d\n", *data_num_per_ch);
	}

	printf("sizeof(short): %lu \n", sizeof(short));

	input_data = (short*)malloc(file_size);
	fread(input_data, sizeof(short), file_size/sizeof(short), file_in);

#ifdef DEBUG
	for ( i=0; i < file_size/sizeof(short); i++) {
		//fread(&(input_data[i]), sizeof(short), 1, file_in);
		printf("Retrieved (%9d)th data: %d\n", i, input_data[i]);
	}
#endif

	return input_data;
}

float* ReadModelInput(char *input_filename, int channel_num, int* data_num_per_ch)
{
	FILE* file_in=NULL;
	char * line = NULL;
	int file_size;
	size_t len = 0;
	ssize_t read;
	float *input_data;
	int i = 0;

	file_in=fopen(input_filename, "r");

	if (file_in==NULL) {
		printf("[FATAL] Fail to open file [%s]\n", input_filename);
		exit(1);
	}
	else {
		fseek (file_in , 0 , SEEK_END);
		file_size = ftell (file_in);
		rewind (file_in);
		*data_num_per_ch = file_size/sizeof(float)/channel_num;
		printf("Input file size       : (%d)bytes \n", file_size);
		printf("Number of input data  : %d\n", file_size/sizeof(float));
		printf("Number of data/channel: %d\n", *data_num_per_ch);
	}

	printf("sizeof(float): %lu \n", sizeof(float));

	input_data = (float*)malloc(file_size);
	fread(input_data, sizeof(float), file_size/sizeof(float), file_in);

#ifdef DEBUG
	for ( i=0; i < file_size/sizeof(float); i++) {
		//fread(&(input_data[i]), sizeof(short), 1, file_in);
		printf("Retrieved (%9d)th data: %f\n", i, input_data[i]);
	}
#endif

	return input_data;
}

void TempDump(char* filename, float* data, int data_num)
{
	FILE* file_out=NULL;
//	int i;

	file_out=fopen(filename, "wb");

	if (file_out==NULL) {
		printf("[FATAL] Fail to open file [%s]\n", filename);
		exit(1);
	}


	fwrite(data, sizeof(float), data_num, file_out);

}
