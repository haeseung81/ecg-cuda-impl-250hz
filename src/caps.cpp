/* *
 * Copyright 1993-2012 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 */


#include "caps.h"
#include "gaussian.h"

#define DBG_FIND_MATCH
#ifdef DBG_FIND_MATCH
#define PNT_FIND_MATCH(...) printf(__VA_ARGS__)
#else
#define PNT_FIND_MATCH(...)
#endif

extern int fail_cnt;


#define TOT_CH_NUM	12
extern CLK base_time; // base clk tick

void FindMatchedQT(float* q_tmpl_data, float* t_tmpl_data , int template_size
		, int* qrs_peakdata, int qrs_data_num
		, float* iir_output_data, int input_num, short indicator
		, int** q_idx_out, int** t_idx_out, int** rr_interval, int iter_cnt)
{
	//int ch_num = 0;
	int idx=0;
	int cnt=0;
	int start = 0;
	int end = 0;
	float* local_data = NULL;
	int max_data_num = 0;
	int q_idx = 0;
	int t_idx = 0;
	int cross_corr_size = 0;



	// template memory allocation;
	(*q_idx_out) = (int*)malloc(sizeof(int)*(qrs_data_num-1));
	(*t_idx_out) = (int*)malloc(sizeof(int)*(qrs_data_num-1));
	(*rr_interval) = (int*)malloc(sizeof(int)*(qrs_data_num-1));

	//
	//	(*template_data) = CropMemAlloc((end-start+1), (template_size/2));
	//	CropData(local_crop_data, start, end, iir_output_data, input_num, indicator, (template_size/2));
	//	local_crop_data = (*template_data);

	// find max memory size for memory allocation
	for(cnt=0; cnt<(qrs_data_num-1); cnt++) {
		if (max_data_num < (qrs_peakdata[cnt+1]-qrs_peakdata[cnt]+1)) {
			max_data_num = (qrs_peakdata[cnt+1]-qrs_peakdata[cnt]+1);
		}
	}

	// memory allocation with maximum data number
	PNT_FIND_MATCH("max_data_num: %d\n", max_data_num);
	local_data = CropMemAlloc(max_data_num, (template_size/2));


	for(cnt=0; cnt<(qrs_data_num-1); cnt++) {
		start = qrs_peakdata[cnt];
		end = qrs_peakdata[cnt+1];
		PNT_FIND_MATCH("----------------------------------------------------------\n");
		PNT_FIND_MATCH("[%d]th beat - start: %d | end; %d | size: %d : input_num: %d\n", cnt+1, start, end , (end-start+1), input_num);

		//printf("[CropData]\n");
		//TempDump("iir_output_data.bin", iir_output_data, input_num*12);

		assert((start-(iter_cnt*input_num))>=0);
		assert((end-(iter_cnt*input_num))>=0);

		if((start-(iter_cnt*input_num))>(template_size/2)){
			if((end-(iter_cnt*input_num)+template_size/2)<input_num){
				CropData(local_data,(start-(iter_cnt*input_num)-template_size/2), (end-(iter_cnt*input_num))+template_size/2, iir_output_data, input_num, indicator, 0);	

			}
		}
		else { 
			CropData(local_data, (start-(iter_cnt*input_num)), (end-(iter_cnt*input_num)), iir_output_data, input_num, indicator, (template_size/2));
		}

		cross_corr_size = (end-start+1) + template_size;

		//TempDump("cropdata.bin", local_data, cross_corr_size*12);

		// Timing Start
		CLK start_time_us = GetTimeUs(base_time);
		// Fineing Q and T index from single beat
		PNT_FIND_MATCH("-------- Find Q --------\n");
		q_idx = FindMaxIndex(q_tmpl_data, template_size, local_data, cross_corr_size, prog_setting.threshold, indicator);

		PNT_FIND_MATCH("-------- Find T --------\n");
		t_idx = FindMaxIndex(t_tmpl_data, template_size, local_data, cross_corr_size, prog_setting.threshold, indicator);
		// Timing end
		CLK elapsed_time_us = GetTimeUs(base_time) - start_time_us;

		printf("[Host] elapsed_time_us: %u\n", elapsed_time_us);

		// offset compensation - subtract padded data and template size/2
		//		q_idx = q_idx + start - (template_size/2);
		//		t_idx = t_idx + start - (template_size/2);
		if (q_idx>-1) { q_idx = q_idx + start-1; }
		if (t_idx>-1) { t_idx = t_idx + start-1; }



		(*q_idx_out)[cnt] = q_idx;
		(*t_idx_out)[cnt] = t_idx;

		PNT_FIND_MATCH(" >>> [%d]th beat - q_idx: %d | t_idx: %d\n\n", cnt+1, q_idx, t_idx);


		// record final Q and T index

		if (cnt<(qrs_data_num-2)) {
			assert(cnt<(qrs_data_num-2));
			(*q_idx_out)[cnt] = q_idx;
			PNT_FIND_MATCH(" >>> [%d]th beat - q_idx: %d\n", cnt, q_idx);
		}
		if (cnt>0) {
			assert(cnt>0);
			(*t_idx_out)[cnt-1] = t_idx;
			PNT_FIND_MATCH(" >>> [%d]th beat - t_idx: %d\n", cnt, t_idx);
		}

		// RR interval data
		if (cnt<(qrs_data_num-2)) {
			(*rr_interval)[cnt] = qrs_peakdata[cnt+2] - qrs_peakdata[cnt+1];
			PNT_FIND_MATCH(" >>> [%d]th beat - rr_interval: %d\n", cnt, (*rr_interval)[cnt]);
		}

		PNT_FIND_MATCH(" >>> [%d]qrs_peakdata: %d\n", cnt, qrs_peakdata[cnt]);
	}

#ifdef DBG_FIND_MATCH
	for (cnt=0; cnt<(qrs_data_num-1); cnt++) {
		printf("q_idx[%d]: %d | t_idx[%d]: %d\n", cnt, (*q_idx_out)[cnt], cnt, (*t_idx_out)[cnt]);
	}
#endif


	//TempDump("crop_data.bin", local_data, ((end-start+1)+(template_size))*12);
}


#define DBG_FIND_MAX_
#ifdef DBG_FIND_MAX
#define PNT_FIND_MAX(...) printf(__VA_ARGS__)
#else
#define PNT_FIND_MAX(...)
#endif



int FindMaxIndex(float* tmpl_data, int template_size, float* crop_data, int crop_size, float threshold, short indicator)
{
	int data_idx = -2;
	int idx=0;
	int cnt=0;
	float cross_corr = 0;
	float max_cross_corr = -1;
	int max_idx = 0;

	float* local_crop = (float*)malloc(sizeof(float)*template_size*12);

	//printf("[FindMaxIndex]\n");



	for (idx=0; idx<(crop_size-template_size); idx++) {
	//for (idx=0; idx<3; idx++) {
		//printf("[%d]th crop from: %d - to : %d\n", idx, idx, (idx+template_size-1));
		CropData(local_crop, idx, (idx+template_size-1), crop_data, crop_size, indicator, 0);

		//TempDump("local_crop.bin", local_crop, crop_size*12);

		for (cnt=0; cnt<GAU_WEIGHT_SIZE*12; cnt++) {
			local_crop[cnt] = local_crop[cnt] * gaussian_weight[cnt%GAU_WEIGHT_SIZE];
			//printf("[%d]th local_crop: %f\n", cnt, local_crop[cnt]);
		}


		cross_corr = CrossCorrelation(tmpl_data, template_size, local_crop, threshold, indicator);



//		if (cross_corr>threshold) {
//			PNT_FIND_MAX("[%d]th cross_corr: %f\n", idx, cross_corr);
//		}

		//printf("[%d]th cross_corr: %f\n", idx, cross_corr);

		if (max_cross_corr<cross_corr) {
			max_cross_corr = cross_corr;
			max_idx = idx;
		}
	}



	if (max_cross_corr>threshold) {
		data_idx = max_idx;
		PNT_FIND_MAX("[FindMaxIndex] max index: %d (%f)\n", data_idx, max_cross_corr);
	}
	else {
		data_idx = -1;
	}

	assert(data_idx>-2);
	return data_idx;
}


float CrossCorrelation(float* tmpl_data, int template_size, float* crop_data, float threshold, short indicator)
{
	float pr_corr = 0;
	float tmpl_avg = 0;
	float crop_avg = 0;

	float corr_value = 0;
	float std_tmpl = 0;
	float std_crop = 0;
	float tmp_t = 0;
	float tmp_c = 0;

	int idx=0;
	int corr_loop_cnt = template_size * 12;

	for (idx=0; idx<corr_loop_cnt; idx++) {
		tmpl_avg += tmpl_data[idx];
		crop_avg += crop_data[idx];
	}

	tmpl_avg = tmpl_avg/corr_loop_cnt;
	crop_avg = crop_avg/corr_loop_cnt;

	for (idx=0; idx<corr_loop_cnt; idx++) {
		tmp_t = tmpl_data[idx] - tmpl_avg;
		tmp_c = crop_data[idx] - crop_avg;

		corr_value = corr_value + (tmp_t * tmp_c);
		std_tmpl = std_tmpl + (tmp_t*tmp_t);
		std_crop = std_crop + (tmp_c*tmp_c);
	}

	assert((std_tmpl*std_crop)>0);
	pr_corr = corr_value / (sqrt(std_tmpl*std_crop));

	return pr_corr;
}


void RefineFindedQT(int* qrs_peakdata, int qrs_data_num, int* q_idx_buf, int* t_idx_buf, int** q_idx, int** t_idx, int** rr_interval)
{
	int idx=0;
	int max_data_size = qrs_data_num-2;
	int cnt=0;
	float rr = 0;

	(*q_idx) = (int*)malloc(sizeof(int)*(qrs_data_num-1));
	(*t_idx) = (int*)malloc(sizeof(int)*(qrs_data_num-1));
	(*rr_interval) = (int*)malloc(sizeof(int)*(qrs_data_num-1));


	printf("[RefineFindedQT] max_data_size: %d\n", max_data_size);

	for(idx=0; idx<max_data_size; idx++) {
		printf(" >>> [%d]th beat - q_idx: %d | t_idx: %d\n", idx, q_idx_buf[idx], t_idx_buf[idx]);
		if (q_idx_buf[idx] != -1 && t_idx_buf[idx+1] != -1) {
			if (q_idx_buf[idx] > t_idx_buf[idx])  {
				if (q_idx_buf[idx] < qrs_peakdata[idx+1] && qrs_peakdata[idx] < t_idx_buf[idx+1]) {
					if (qrs_peakdata[idx+2] < t_idx_buf[idx+1]) {
						cnt++;
						printf(" >>> [%d]th beat - q_idx: %d | t_idx: %d", idx, q_idx_buf[idx], t_idx_buf[idx]);
						(*rr_interval)[idx] = qrs_peakdata[idx+2] - qrs_peakdata[idx+1];
					}
					else {

					}
				}
				else {

				}
			}
			else {
				printf(" - need to move t_idx data");
				printf(" >>> [%d]th beat - q_idx: %d | t_idx: %d", idx, q_idx_buf[idx], t_idx_buf[idx-1]);
				(*rr_interval)[idx] = qrs_peakdata[idx+2] - qrs_peakdata[idx+1];
			}
		}

		printf("\n");
	}








}




int compare_function(const void *a,const void *b) {
	int x = *((int *)a);
	int y = *((int *)b);

	if (x>y) {return 1;}
	if (x<y) {return -1;}
	return 0;
}


#define DBG_QTC_VERIFY_
#ifdef DBG_QTC_VERIFY
#define PNT_QTC_VERIFY(...) printf(__VA_ARGS__)
#else
#define PNT_QTC_VERIFY(...)
#endif

void QTdataVerification(int* q_idx_in, int* t_idx_in, int* rr_interval, int idx_num, int sampling_rate
		, float** qtc_output, float* avg_qtc, float* median_qtc
		, float* avg_heartrate, int* heartrate_num, float* abnormal_ratio, float* heartrate_max, float* heartrate_min)
{
	int cnt=0;
	int ab_cnt=0;
	int idx=0;
	int median_idx = 0;

	float rr_interval_ms = 0;
	float qtc_value = 0;
	float qtc_sum = 0;
	float heartrate_sum = 0;
	float hr_min = 300; // temp heartrate min
	float hr_max = 0; // temp heartrate max
	float curr_hr = 0; // current heartrate

	// memory allocation for qtc_output ()
	(*qtc_output) = (float*)malloc(sizeof(float)*idx_num);



	for (idx=0; idx<idx_num; idx++) {
		if (q_idx_in[idx]>0 && t_idx_in[idx]>0) {

			PNT_QTC_VERIFY("(%d)th qtc - index number pass\n", idx);

			rr_interval_ms = ((float)rr_interval[idx])/sampling_rate;
			if (0.33<rr_interval_ms && rr_interval_ms<1.00) {

				PNT_QTC_VERIFY("(%d)th qtc - RR interval(ms) pass: %f\n", idx, rr_interval_ms);

				// increase correct data count;
				qtc_value = (((float)(t_idx_in[idx] - q_idx_in[idx]))/sampling_rate)/sqrt(rr_interval_ms);
				if (0.30<qtc_value && qtc_value<0.60) {

					PNT_QTC_VERIFY("(%d)th qtc - QTC value pass: %f\n", idx, qtc_value);

					(*qtc_output)[cnt] = qtc_value;
					qtc_sum = qtc_sum + qtc_value;

					curr_hr = ((float)60/rr_interval_ms);
					heartrate_sum = heartrate_sum + curr_hr;

					if (hr_min>curr_hr) { hr_min = curr_hr;	}
					if (hr_max<curr_hr) { hr_max = curr_hr; }
					if (qtc_value>0.45) { ab_cnt++; }

					PNT_QTC_VERIFY("(%d)th qtc: %f\n", cnt, qtc_value);

					cnt++;
				}
				else {
					PNT_QTC_VERIFY("(%d)th qtc - QTC value FAIL: %f\n", idx, qtc_value);
				}

			}
			else {
				PNT_QTC_VERIFY("(%d)th qtc - RR interval(ms) FAIL: %f\n", idx, rr_interval_ms);
			}
		}
		else {
			PNT_QTC_VERIFY("(%d)th qtc - index number FAIL\n", idx);
		}
		PNT_QTC_VERIFY("\n");
	}

	//
	if (cnt>0) {
		*avg_qtc = qtc_sum/cnt;
		*avg_heartrate = heartrate_sum/cnt;
		*heartrate_num = cnt+1;
		*heartrate_max = hr_max;
		*heartrate_min = hr_min;
		*abnormal_ratio =  ( ((float)ab_cnt)/cnt )*100;
	}


	qsort((*qtc_output), cnt, sizeof(float), compare_function);

	//printf("===========================\n");
	for (idx=0; idx<cnt; idx++) {
		printf("[RESULT:QTC-SORTED] (%d)th qtc: %f\n", idx, (*qtc_output)[idx]);
	}

	median_idx = cnt/2;
	*median_qtc = (*qtc_output)[median_idx];

	if (0.3<=*median_qtc && *median_qtc<=0.6) { printf("[RESULT:QTC-MEDIAN] Median qtc: %f (%d)\n", *median_qtc, median_idx); }
	else { fail_cnt++; printf("[RESULT:QTC-MEDIAN] FAIL!! (fail_cnt: %d)\n", fail_cnt); }


}




