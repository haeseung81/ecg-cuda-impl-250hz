/*
 * filter.c
 *
 * Code generation for function 'filter'
 *
 * C source code generated on: Mon Jan 12 12:33:15 2015
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "IIR_bandpassfilter.h"
#include "filter.h"
#include "IIR_bandpassfilter_emxutil.h"

/* Function Definitions */
void filter(emxArray_real_T *x, real_T zi[10], emxArray_real_T *y, real_T* coeff_a, real_T* coeff_b)
{
	uint32_T unnamed_idx_0;
	int32_T j;
	real_T dbuffer[11];
	int32_T k;

	// coeff b
//	static const real_T dv0[11] = { 0.34657209275210371, 0.0, -1.7328604637605187,
//			0.0, 3.4657209275210374, 0.0, -3.4657209275210374, 0.0, 1.7328604637605187,
//			0.0, -0.34657209275210371 };
	real_T* dv0 = coeff_b;

	real_T b_dbuffer;

	// coeff a
//	static const real_T dv1[11] = { 1.0, -1.9864100375624976, -1.1045965530105768,
//			3.1803012204716605, 1.5015119943422237, -2.9023550769152804,
//			-1.2223848982942684, 1.2978851964386844, 0.59478413479107517,
//			-0.23862570844921471, -0.12011026830057617 };
	real_T* dv1 = coeff_a;

	unnamed_idx_0 = (uint32_T)x->size[0];
	j = y->size[0];
	y->size[0] = (int32_T)unnamed_idx_0;
	emxEnsureCapacity((emxArray__common *)y, j, (int32_T)sizeof(real_T));
	memcpy(&dbuffer[1], &zi[0], 10U * sizeof(real_T));
	for (j = 0; j + 1 <= x->size[0]; j++) {
		for (k = 0; k < 10; k++) {
			dbuffer[k] = dbuffer[k + 1]; // shift left
		}

		dbuffer[10] = 0.0;
		for (k = 0; k < 11; k++) {
			b_dbuffer = dbuffer[k] + x->data[j] * dv0[k];
			dbuffer[k] = b_dbuffer;
		}

		for (k = 0; k < 10; k++) {
			dbuffer[k + 1] -= dbuffer[0] * dv1[k + 1];
		}

		y->data[j] = dbuffer[0];
	}
}

/* End of code generation (filter.c) */
